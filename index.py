from dotenv import load_dotenv

load_dotenv()

from src.app import main


if __name__ == "__main__":
    c = True
    while c:
        try:
            main()
            c = False
        except Exception as e:
            c = True
