from datetime import datetime
import time
import os

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def get_driver():
    driver_path = os.getenv("DRIVER_PATH")
    if not driver_path:
        raise Exception

    driver = webdriver.Edge(driver_path, keep_alive=True)
    driver.get("https://citas.sre.gob.mx/")

    return driver


def main():
    driver = get_driver()
    modal_btns = WebDriverWait(driver, 30).until(
        EC.presence_of_all_elements_located((By.CSS_SELECTOR, ".modal-body .btn"))
    )
    modal_btns[0].click()
    time.sleep(5)

    modal_btns = WebDriverWait(driver, 30).until(
        EC.presence_of_all_elements_located((By.CSS_SELECTOR, ".modal-body .btn"))
    )
    modal_btns[1].click()

    inputs = WebDriverWait(driver, 30).until(
        EC.presence_of_all_elements_located((By.CSS_SELECTOR, ".form-control"))
    )

    email = os.getenv("EMAIL") or ""
    password = os.getenv("PASS") or ""
    inputs[0].send_keys(email)
    inputs[1].send_keys(password)

    checkbox = WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'input[value="One"]'))
    )
    checkbox.click()
    time.sleep(2)

    modal_close_btn = WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, ".modal-body svg"))
    )
    modal_close_btn.click()

    login_btn = WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, ".btn.pull-right"))
    )
    login_btn.click()
    time.sleep(5)

    modal_close_btn = WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, ".modal-body svg"))
    )
    modal_close_btn.click()

    program_btn = WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, ".btn.btn-primary"))
    )
    program_btn.click()


    while True:
        time.sleep(3)
        modal_close_btn = WebDriverWait(driver, 30).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, ".modal-body svg"))
        )
        modal_close_btn.click()

        now = datetime.now()
        oficinas = driver.find_elements(By.CLASS_NAME, "card")
        if len(oficinas) > 0:
            print(f"[{now}] Encontrado")
            break

        input_select = driver.find_element(By.CSS_SELECTOR, 'input[aria-labelledby="vs2__combobox"]')
        input_select.click()
        time.sleep(3)
        option_nl = driver.find_element(By.ID, "vs2__option-18")
        option_nl.click()
        try:
            oficinas = WebDriverWait(driver, 10).until(
                EC.presence_of_all_elements_located((By.CLASS_NAME, "card"))
            )
            if len(oficinas) > 0:
                print(f"[{now}] Encontrado")
                break
        except:
            pass

        print(f"[{now}] No encontrado")
        time.sleep(30)
        driver.refresh()

